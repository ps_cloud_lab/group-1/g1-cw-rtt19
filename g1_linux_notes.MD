# LINUX

A Linux system is divided into three main parts:

* Hardware     - This includes all the hardware that your system runs on as well as memory, CPU, disks.
* Linux Kernel - The Kernel manages the hardware and tells it how to interact with the system.It is the core of the operating system.
* Terminal     - This is where users directly interact with the system

# Linux Distributions

* Ubuntu
* Debian
* Red Hat Enterprise Linux
* Fedora
* Linux Mint

# Shell

The shell is a program that takes commands from the keyboard and sends them to the operating system to perform. The terminal or console is a program that launchs a shell for you. 

Almost all Linux distributions will default to the Borne Again Shell (Bash)

# Linux File System Hierarchy

Everything in linux is a file.Every file is organized in a hierarchical directory tree. The first directory in the filesystem is named the root directory. The root directory has many folders and files which you can store more folders and files.An operating system stores data on disk drives using structure called a file system consisting of files, directories and information needed to access and locate them. Many different types of files systems are present like EXT2, EXT3, XFS, . Linux file system store information in hierarchy of directories and files


<img src="/g1_linux_images/linux_file_system.png" />

# /Root

* Primary hierarchy root and root directory of the entire file system hierarchy.
* Every single file and directory starts from the root directory
* /root is the root user’s home directory, which is not the same as /

* /boot : contains file that is used by the boot loader (grub.cfg)
* /root : root user home directory. It’s not the same as /
* /dev : System Devices
* /etc : Configuration files
* /bin: Everyday user commands
* /sbin: System or file system commands
* /opt: Optional add on apps
* /proc: Running processes (only exist in memory)
* /lib: C program library files needed by commands
* /tmp: directory for temporary files
* /home: directory for user • /var: system logs
* /run: System daemons that run/ start very early to store tmp runtime files like PID files
* /mnt : to mount external file systems (Ex. NFS)
* /media: for CDROM Mounts

# Filesystem paths
2 paths to navigate file system
* Absolute path : begins with a “/” . from the root directory
* Relative Path: to navigate to folders in the existing folder

# Directory listing attributes:  

* Type of file
* Permissions
* Number of Links
* Owner
* Group
* Size
* Month
* Day
* Time
* Name of file

# Commands

Few points to remember:
* Commands are case sensitive
* In Commands we can include options and inputs 
* Options change the behavior of a command (ex. ls and ls -a)
* Commands operate on inputs (ex. echo and echo “Hello World”)
* Command names need to be on shell’s search path (echo $PATH)
* Command options you can either issue with long form or short form. For short form, you can merge multiple options. Not possible   with long forms. 
* Tab Completion – autocompletes the commands. 
* To find commands that begin with letter t, press t and tab twice

# Navigating file System

<img src="/g1_linux_images/Navigating_the_file_system.png" />

# Hidden Files

Hidden files are the files that contain dot/period at the beginning of the filename. To create a hidden file in Linux, preceed the file name with a period .
Every inode in the Linux structure has a unique number identified with it. It is also called the index number and has the following attributes:
* Size
* Owner
* Date/time
* Permissions and access control
* Location on the disk
* File types
* Number of links
* Additional metadata about the file

<img src="/Calendar%20and%20dates.png" />

history – History command. Contains all the history of command executed so far

!45            : The command at 45th location gets executed
!!             : Most recent command in list gets executed
history -c -w  :Clears the history and writes changes

which          :To see where a command is located
which man      : Shows the location of command

man - How to use each command
man ls         : Manual page to command ls
man echo       : Manual page to echo command

# Creating Directories and files

<img src="/Creating%20dir%20commands.png" />
<img src="/Creating%20dir%20commands2.png" />

# Copying files, moving files and directories:

<img src="/Copying%20file.1.png" />




 











